<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class dbcreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new MySQL database based on config file or the provided parameter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * we build the database
     * @return int
     */
    public function handle()
    {
        //we get the name of the database
        $schemaName = $this->argument('name') ?: config('database.connections.mysql.database');
        //we get the charset of the database
        $charset = config('database.connections.mysql.charset', 'utf8mb4');
        //we get the general charset of the database
        $collation = config('database.connections.mysql.collation', 'utf8mb4_general_ci');
        //we empty temporarily the database
        config(['database.connections.mysql.database' => null]);
        //we drop the database
        $query = "DROP DATABASE IF EXISTS $schemaName;";
        //command who drop the database
        DB::statement($query);
        //we create the database if she not exists
        $query = "CREATE DATABASE IF NOT EXISTS $schemaName CHARACTER SET $charset COLLATE $collation;";
        //command who drop the database
        DB::statement($query);

        echo "Database $schemaName created successfully";
        //we set the shemaname database
        config(['database.connections.mysql.database' => $schemaName]);





        config(['database.connections.mysql.database' => $schemaName]);

    }
}